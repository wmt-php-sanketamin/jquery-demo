    
const json1 = fetch('https://api.webmobtech.biz/task')
    .then(response => response.json())
    .then(data => 
        {
            const arr1 = _.cloneDeep(data.data.purchased_services);
            const arr2 = _.cloneDeep(data.data.purchased_services);
            
            const add1 = arr1.map((service) => {
                let filter_ser = service.purchased_office_template.purchased_office_services.filter((checkserv) => checkserv.service_selected);
                service.purchased_office_template.purchased_office_services = filter_ser.slice();
                return service;
            })
            .filter((service) => 
                service.purchased_office_template.purchased_office_services.length
            );    
    
            const  add2 = arr2.map((add_service) => {
                let filter_ser = add_service.purchased_office_template.purchased_office_services.filter((checkserv) => !(checkserv.service_selected));
                add_service.purchased_office_template.purchased_office_services = filter_ser.slice();
                return add_service;
            })
            .filter((add_service) => 
                add_service.purchased_office_template.purchased_office_services.length
            )


            $.map(add1, (value) => {
                let ex = $(`<div class = "Services" ></div>`);
                let heading = $(`<h4> Main Service ${value.id} </h4>`);
                ex.append(heading);

                $.map(value.purchased_office_template.purchased_office_services, (service) => {
                    let maindiv = $(`
                        <div class = "pur_services">
                        <div class="p1"><img src="https://picsum.photos/100" alt="img" class="imgs" /></div>
                        <div class = "p2">
                        <div class = "p2-1"> <div>Service ${service.id}</div> <div>Kr ${service.price}/-</div></div>
                        <div class="p2-2"><p>${service.description}</p></div></div>
                    `);
                    ex.append(maindiv);
                })
                $(".mainservice").append(ex);

                let ex1 = $(`<div class = "inside"></div>`);
                $.map(value.purchased_office_template.purchased_office_services, (service) => {

                    let calculate = $(`
                        <div class="inside_1"> Service ${service.id} </div>
                        <div class="inside_2"> Kr  ${service.price} /-</div>
                    `);
                    ex1.append(calculate);

                })
                $(".calculation").append(ex1);
            })

            let total_price = add1.reduce((acc1,services1) => {
                let var2 = services1.purchased_office_template.purchased_office_services.map(value => {
                    let Price = parseFloat(value.price);
                    return Price;
                });
                let var3 = var2.reduce((total,value) => {
                    return total += parseFloat(value);
                },0.0)
                return acc1 + var3;                
            },0.0)

            let ex2 = $(`<div class = "inside"></div>`);
                let total_price1 = $(`
                    <div class="inside_1"> Total Price </div>
                    <div class="inside_2"> Kr  ${total_price} /-</div>
                    `);
                    ex2.append(total_price1);

                    $(".calculation").append(ex2);

            $.map(add2, (value) => {
                let ex = $(`<div class = "Services" ></div>`);
                let heading = $(`<h4> Main Service ${value.id} </h4>`);
                ex.append(heading);

                $.map(value.purchased_office_template.purchased_office_services, (service) => {
                    let maindiv = $(`
                        <div class = "pur_services">
                        <div class="p1"><img src="https://picsum.photos/100" alt="img" class="imgs" /></div>
                        <div class = "p2">
                        <div class = "p2-1"> <div>Service ${service.id}</div> <div>Kr ${service.price}/-</div></div>
                        <div class="p2-2"><p>${service.description}</p></div></div>
                    `);
                    ex.append(maindiv);
                })
                $(".mainservice2").append(ex);

                
                $.map(value.purchased_office_template.purchased_office_services, (service) => {

                    let calculate = $(`
                    <div class="inside">    
                        <div class="inside_1"> Service ${service.id} </div>
                        <div class="inside_2"> Kr  ${service.price} /-</div>
                        </div>
                    `);
                    $(".calculation2").append(calculate);

                })
                
            })
            let add_total_price = add2.reduce((acc2,services2) => {
                let var2 = services2.purchased_office_template.purchased_office_services.map(value => {
                    let Price = parseFloat(value.price);
                    return Price;
                });
                let var3 = var2.reduce((total,value) => {
                    return total += parseFloat(value);
                },0.0)
                return acc2 + var3;                
            },0.0)

            let add_total_price1 = $(`
            <div class="inside"> 
                <div class="inside_1"> Total Price </div>
                <div class="inside_2"> Kr  ${add_total_price} /-</div>
                </div>
                `);
                $(".calculation2").append(add_total_price1);

        }
    );
    
