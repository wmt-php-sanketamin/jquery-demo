$(document).ready(function() {

    $('#first_form').submit(function(e) {
      e.preventDefault();
      var first_name = $('#first_name').val();
      var last_name = $('#last_name').val();
      var email = $('#email').val();
      var pwd = $('#pwd').val();
  
      $(".error").remove();
  
      if (first_name.length < 1) {
        $('#first_name').after('<span class="error">This field is required</span>');
      }
      if (last_name.length < 1) {
        $('#last_name').after('<span class="error">This field is required</span>');
      }
      if (email.length < 1) {
        $('#email').after('<span class="error">This field is required</span>');
      } else {
        var regEx = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}/;
        
        var validEmail = regEx.test(email);
        if (!validEmail) {
          $('#email').after('<span class="error">Enter a valid email</span>');
        }
      }
      if (pwd.length < 8) {
        $('#pwd').after('<span class="error">password must be at least 8 characters long</span>');
      }
    });
    function checkPasswordMatch() {
        var password = $("#pwd").val();
        var confirmPassword = $("#re-pwd").val();
        if (password != confirmPassword)
            $("#CheckPasswordMatch").html("Passwords does not match!");
        else
            $("#CheckPasswordMatch").html("Passwords match.");
    }
       $("#re-pwd").keyup(checkPasswordMatch);
  });