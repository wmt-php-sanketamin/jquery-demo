$(document).ready(function(){
  $("button").click(function(){
    var div = $(".circle");
    div.animate({left: '200px',top: '0px', opacity: '0.5'}, "slow");
    div.animate({left: '200px', top: '200px', opacity: '0.8'}, "slow");
    div.animate({left: '0px', top: '200px', opacity: '0.5'}, "slow");
    div.animate({left: '0px', top: '0px', opacity: '0.8'}, "slow");
  });
  $(".rotate").hover(function(){
    $(".circle").stop();
  });
});